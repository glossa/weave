(ns build
  (:require
    [clojure.string :as str]
    [clojure.tools.build.api :as b]))

#_"Clojure data format and tools for weaving documents"
(def lib 'dev.glossa/weave)
(def -version (str (str/trim (slurp "build/VERSION_PREFIX")) (b/git-count-revs nil)))
(def target-dir "target")
(def class-dir (str target-dir "/" "classes"))
(def jar-file (format "%s/%s-%s.jar" target-dir (name lib) -version))
(def src ["src"])
(def basis (b/create-basis {:aliases nil
                            :extra nil
                            :project "./deps.edn"
                            :user nil}))

;; Adapted from git-count-revs implementation.
(def git-head-sha
  (-> {:command-args ["git" "rev-parse" "HEAD"]
       :dir (.getPath (b/resolve-path "."))
       :out :capture}
      b/process
      :out
      str/trim))

(def git-tag (str "v" -version))

(def scm
  {:connection "scm:git:git://gitlab.com/glossa/weave.git"
   :developerConnection "scm:git:ssh://git@gitlab.com/glossa/weave.git"
   :tag git-tag
   :url "https://gitlab.com/glossa/weave"})

(defn artifact-name
  "Print name of the JAR artifact (without extension)"
  [_]
  (println (name lib)))

(defn clean
  "Delete the build target directory"
  [_]
  (println (str "Deleting contents of " target-dir "..."))
  (b/delete {:path target-dir}))

(defn jar
  "Create the jar from a source pom and source files"
  [_]
  (println (str "Building JAR and POM for version " -version "..."))
  (b/write-pom {:basis basis
                :class-dir class-dir
                :lib lib
                :scm scm
                :src-dirs src
                :src-pom "pom.xml"
                :version -version})
  (b/copy-dir {:src-dirs src
               :target-dir class-dir})
  (b/jar {:class-dir class-dir
          :jar-file jar-file}))

(defn sha
  "Print Git SHA for HEAD of current branch."
  [_]
  (println git-head-sha))

(defn tag
  "Print Git tag for this build."
  [_]
  (println git-tag))

(defn version
  "Print version"
  [_]
  (println -version))
