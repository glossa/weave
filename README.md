# Weave #

The Glossa Weave library defines an API for transforming documents written as trees of Clojure data into human-readable strings.

```clj
;; Git dep
{dev.glossa/weave
 {:git/url "https://gitlab.com/glossa/weave.git"
  :git/tag "v0.2.132"
  :git/sha "3fe90fdf45fe30608b0abb119ba0450882c18041"}}

;; Maven dep at https://clojars.org/dev.glossa/weave
{dev.glossa/weave {:mvn/version "0.2.132"}}
```

**No output format** is provided as part of this library. Add [Weave Markdown](https://gitlab.com/glossa/weave-markdown) to your classpath to get simple Markdown output for `(glossa.weave/weave your-doc)`

## Development ##

_NB: Review the scripts and `deps.edn` files for necessary aliases (not all of which are defined in this repo)._

Please run the setup script to populate Git hooks:

```shell
./script/setup
```

To start nREPL (bring your own `:cider` alias):

```shell
./script/dev
```

To run tests:

```shell
./script/test
```

To run everything that CI does:

```shell
./script/ci
```

## License ##

Copyright © Daniel Gregoire, 2021

THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THIS ECLIPSE PUBLIC LICENSE ("AGREEMENT"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT.
