(ns glossa.weave.cli
  (:require
   [clojure.string :as str]
   [glossa.weave :as weave]))

(defn- render-doc*
  [doc-tree]
  (weave/weave doc-tree))

(defn render-doc
  [opts]
  (let [{:keys [source target]} opts]
    (if-let [source-var (requiring-resolve source)]
      (let [source-value @source-var
            doc (if (fn? source-value)
                  (source-value)
                  source-value)
            doc-contents (render-doc* doc)]
        (if target
          (spit (str target) doc-contents)
          (println doc-contents)))
      (binding [*err* *out*]
        (println "[FAILED] Could not find:" source)))))

(defn help
  [_]
  (let [msg (str/join "\n"
                      ["Available functions:"
                       ""
                       "   - render-doc"
                       "      - clj -X glossa.weave.cli/render-doc :source your-namespace/your-var :target README.md"])]
    (println msg)))
