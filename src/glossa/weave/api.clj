(ns glossa.weave.api
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [glossa.weave.ir :as ir]))

;;
;; Parse
;;

(defn- parse*
  [node]
  (first node))

(defmulti parse #'parse*)

(defn parse-shallow
  "Parse without recursively parsing the children."
  [node]
  (let [[tag & attr-kids] node
        [primus & kids] attr-kids
        [attrs children] (if (map? primus)
                           [primus kids]
                           [{} attr-kids])]
    {:tag tag
     :attrs attrs
     :content (vec children)}))

(defmethod parse :default
  [node]
  (update (parse-shallow node)
          :content
          (fn [content]
            (into []
                  (comp (map (fn [child]
                               (cond
                                 (nil? child)
                                 nil

                                 (sequential? child)
                                 (parse child)

                                 :else child)))
                        (remove nil?))
                  content))))

;;
;; Emit
;;

(defn- emit*
  [env _ir node]
  (if (map? node)
    [(:tag node) (:output env)]
    (type node)))

(defmulti emit #'emit*)

(defn node
  ([tag] (node tag []))
  ([tag content] (node tag {} content))
  ([tag attrs content]
   {:tag tag :attrs attrs :content content}))

;; TODO Here and elsewhere, provide both 'glossa.weave and shorter 'weave namespace support.
;; TODO Catalog from section 4 of https://html.spec.whatwg.org/multipage/
;; TODO Support first pass of https://developer.mozilla.org/en-US/docs/Web/HTML/Element
;; TODO Consider HTML-facing Markdown version that liberally emits HTML
;; TODO Consider whether this should live in the Markdown-specific project or is fine here. Having a core, shared porcelain set of keywords and default IR mappings is convenient, for now.
(def ir-mapping
  {:glossa.weave/doc ::ir/doc

   , :a {:ir-tag ::ir/link :ir-attrs {::rename-keys {:href :url}}}
   :abbr ::ir/abbreviation
   :address ::ir/contact
   :article ::ir/article
   :aside ::ir/aside
   , :b ::ir/bold
   , :blockquote ::ir/quote-paragraph
   :br ::ir/line-break
   :caption ::ir/caption
   , :chapter ::ir/document-chapter
   :cite ::ir/cite
   , :code ::ir/literal
   , :code-whitespace ::ir/literal-show-whitespace
   :data ::ir/data ; TODO attr of value for machine-readable
   , :dd ::ir/description-definition
   :del ::ir/delete-text
   :details ::ir/details
   :dfn ::ir/definition
   , :div ::ir/division
   , :dl ::ir/description-list
   , :dt ::ir/description-term
   , :em ::ir/emphasis
   , :emph ::ir/emphasis
   :endnote ::ir/endnote
   , :enumerate ::ir/ordered-list
   , :env ::ir/environment
   :figure ::ir/figure
   :figcaption ::ir/figure-caption
   :footer ::ir/footer
   :footnote ::ir/footnote
   , :h1 {:ir-tag ::ir/heading :ir-attrs {:level 0}}
   , :h2 {:ir-tag ::ir/heading :ir-attrs {:level 1}}
   , :h3 {:ir-tag ::ir/heading :ir-attrs {:level 2}}
   , :h4 {:ir-tag ::ir/heading :ir-attrs {:level 3}}
   , :h5 {:ir-tag ::ir/heading :ir-attrs {:level 4}}
   , :h6 {:ir-tag ::ir/heading :ir-attrs {:level 5}}
   :header ::ir/header ; TODO Consider global header vs. running header (posts vs. books). Same for footer.
   :hgroup ::ir/header-group
   :highlight ::ir/highlight ; TODO This is HTML's mark. Consider mark vs. label vs. highlight
   , :hr ::ir/horizontal-line
   , :hline ::ir/horizontal-line
   , :it ::ir/italic
   , :i ::ir/italic
   :ins ::ir/insert-text
   , :item ::ir/list-item
   , :itemize ::ir/unordered-list
   :kbd ::ir/keyboard
   , :label ::ir/label ; TODO Never been fond of label as a term for this. Considering 'mark'.
   , :li ::ir/list-item
   :main ::ir/main
   :math ::ir/math ; NOTE: Thinking $ for LaTeX, MathML (or other?) for HTML
   :nav ::ir/nav
   , :norm ::ir/string
   , :ol ::ir/ordered-list
   , :part ::ir/document-part
   , :p ::ir/paragraph

   :paragraph ::ir/document-paragraph
   :subparagraph ::ir/document-subparagraph

   , :pageref ::ir/page-reference
   , :pre ::ir/literal-paragraph
   , :pre-whitespace ::ir/literal-paragraph-show-whitespace
   :q ::ir/quote
   :quote ::ir/quote
   , :ref ::ir/reference
   , :s ::ir/strike-through
   :samp ::ir/sample
   :section ::ir/document-section
   :small ::ir/fine-print
   :span ::ir/span
   :subsection ::ir/document-subsection
   :subsubsection ::ir/document-subsubsection
   :summary ::ir/summary
   , :strong ::ir/bold
   :sub ::ir/subscript
   :sup ::ir/superscript
   :table ::ir/table
   :time ::ir/time ; TODO attr of datetime for machine-readable
   :toc ::ir/table-of-contents
   , :textbf ::ir/bold
   , :textit ::ir/italic
   , :textrm ::ir/string
   , :texttt ::ir/literal
   , :ul ::ir/unordered-list
   :var ::ir/variable
   , :verbatim ::ir/literal-paragraph
   , :verbatim* ::ir/literal-paragraph-show-whitespace})

(assert
 (every?
  (fn [[_k v]]
    (or (qualified-ident? v)
        (and (map? v)
             (every? (partial contains? v) [:ir-tag :ir-attrs]))))
  ir-mapping))

(defn emits-ir?
  [porcelain ir mapping]
  (let [ir-index (reduce-kv
                  (fn [m k v]
                    (update m v (fnil conj #{}) k))
                  {}
                  mapping)]
    (contains? (get ir-index ir) porcelain)))

(deftype NoContent [])
(def ^:private no-content-node (NoContent.))

(defn- emit-default*
  [env ir ir-tag {:keys [attrs content]}]
  (when (string? content)
    (throw (ex-info "Node :content must be a collection. Found a string."
                    {:content content
                     :error :content-not-collection})))
  (let [content (if (seq content)
                  content
                  [no-content-node])
        {:keys [env ir]} (reduce
                          (fn [{:keys [env ir]} x]
                            (emit env ir x))
                          {:env env
                           :ir  ir}
                          content)]
    {:env env
     :ir  (into ir [attrs ir-tag])}))

(defn emit-default
  [env ir {:keys [tag] :as node}]
  (let [ir-mapping (or (:ir-mapping env) ir-mapping)
        ir-spec (if (qualified-ident? tag)
                  (get ir-mapping tag tag)
                  (get ir-mapping tag ::ir/debug))
        node (if (= ir-spec ::ir/debug)
               (assoc-in node [:attrs ::ir/unhandled-tag] tag)
               node)]
    (if (map? ir-spec)
      (let [{:keys [ir-tag ir-attrs]} ir-spec
            attrs (:attrs node)
            attrs (if-let [m (::rename-keys ir-attrs)]
                    (set/rename-keys attrs m)
                    attrs)
            ;; TODO Consider whether or how to handle collisions here. Likely unintentional and confusing to end-user.
            attrs (merge attrs (dissoc ir-attrs ::rename-keys))]
        (emit-default* env ir ir-tag (assoc node :attrs attrs)))
      (emit-default* env ir ir-spec node))))

(defmethod emit :default
  [env ir node]
  (if (map? node)
    (emit-default env ir node)
    {:env env
     :ir  (conj ir (pr-str node))}))

(defmethod emit String
  [env ir string]
  {:env env
   :ir  (conj ir string {} ::ir/string)})

(defmethod emit NoContent
  ;; Added by compilation to ensure all nodes have children for consistency.
  [env ir _]
  {:env env
   :ir  (conj ir ::ir/no-content)})

;;
;; Render
;;

(defn pop1
  [stack]
  (let [item-0 (peek stack)
        stack (pop stack)]
    [stack item-0]))

(defn pop2
  [stack]
  (let [item-0 (peek stack)
        stack (pop stack)
        item-1 (peek stack)
        stack (pop stack)]
    [stack [item-0 item-1]]))

(defn push
  [stack ir-instruction]
  (conj stack ir-instruction))

;; TODO Still not convinced output-specification is best in the dispatch value of the multimethod. But want i18n front and center.
(defn- render*
  [env _stack ir-instruction]
  [ir-instruction (:output env)])

;; TODO Inheritance. How to handle case of folks wanting everything but overriding a small number of evaluations?
;;      Inheritance is probably not the best answer there. Perhaps better config threading.
;;      Main use-case that I envision is i18n (e.g., needing to change quote marks, currency, date formats, etc.)
;;      For now, going to capture as much of the default implementation as possible as separate, simple functions
;;      that the multimethods call, so that folks can easily copy and paste or, if we decide later to make those
;;      functions public, folks can reuse public functions.
(defmulti render
  "Given the `env`, the data `stack`, and an `ir` instruction, interpret
  the instruction to produce a two-item tuple of a new `env` and new `stack`.
  The stack will consist of the individual parts of the finished document
  which will subsequently be stitched; the env is an out-of-band map of values
  that contains important dispatch data (e.g., `:output`) but can also contain
  node-specific data (e.g., test output when run in a test mode)."
  #'render*)

(defn render-default
  [env stack xs]
  (if (string? xs)
    {:env   env
     :stack (conj stack xs)}
    {:env   env
     :stack (apply conj stack xs)}))

(defmethod render :default
  [env stack x]
  ;; NOTE: Because ::ir/debug transcends format.
  (if (= x ::ir/debug)
    (let [[stack [attrs content]] (pop2 stack)]
      (render-default env stack (str "**WEAVE DEBUG** ATTRS: " (pr-str attrs) " // CONTENT: " (pr-str content) "\n\n")))
    {:env   env
     :stack (conj stack x)}))

;;
;; Env
;;

(defn stitch-default
  [stack]
  (as-> stack $
    (str/join "" $)
    (str/trimr $)
    (str $ "\n")))

(def default-env
  {:output 'glossa.weave.output.markdown/output-format
   :ir-mapping ir-mapping
   :stitch-fn stitch-default})

(defn- resolve-env
  [env]
  (reduce-kv
   (fn [m k v]
     (if (qualified-symbol? v)
       (if-let [vr (requiring-resolve v)]
         (assoc m k (deref vr))
         (if (= v (:output default-env))
           (throw (ex-info "Add dev.glossa/weave-markdown as a dependency to your project."
                           {:missing-dependency 'dev.glossa/weave-markdown}))
           (assoc m k v)))
       (assoc m k v)))
   {}
   env))

(defn env
  "Return a Weave env. Optional `env-map` is simply merged with defaults if provided."
  ([] (env {}))
  ([env-map]
   (resolve-env (merge default-env env-map))))
